EESchema Schematic File Version 4
LIBS:hAI-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "hAI - GreenHouse"
Date "2022-01-16"
Rev "0.1"
Comp "HaYu"
Comment1 "HaWay"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x03_Female DHT22
U 1 1 61E417EE
P 2900 2800
F 0 "DHT22" H 2927 2826 50  0000 L CNN
F 1 "DHT22" H 2927 2735 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 2900 2800 50  0001 C CNN
F 3 "~" H 2900 2800 50  0001 C CNN
	1    2900 2800
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x04_Female LCD1
U 1 1 61E418FF
P 2900 2250
F 0 "LCD1" H 2927 2226 50  0000 L CNN
F 1 "LCD" H 2927 2135 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2900 2250 50  0001 C CNN
F 3 "~" H 2900 2250 50  0001 C CNN
	1    2900 2250
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x04_Female M44009
U 1 1 61E419A3
P 2900 3350
F 0 "M44009" H 2927 3326 50  0000 L CNN
F 1 "MAX44009" H 2927 3235 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2900 3350 50  0001 C CNN
F 3 "~" H 2900 3350 50  0001 C CNN
	1    2900 3350
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x06_Female RELAY1
U 1 1 61E41B35
P 6000 3000
F 0 "RELAY1" H 6027 2976 50  0000 L CNN
F 1 "Relay" H 6027 2885 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 6000 3000 50  0001 C CNN
F 3 "~" H 6000 3000 50  0001 C CNN
	1    6000 3000
	1    0    0    -1  
$EndComp
Text GLabel 4200 2400 0    50   Output ~ 0
SDA
Text GLabel 4200 2500 0    50   Output ~ 0
SCL
Text GLabel 3100 2350 2    50   Input ~ 0
5V
Text GLabel 3100 2050 2    50   Input ~ 0
GND
Text GLabel 4200 1150 0    50   Input ~ 0
5V
Text GLabel 4200 1250 0    50   Input ~ 0
GND
Wire Wire Line
	4200 1150 4400 1150
Text GLabel 3100 2250 2    50   Input ~ 0
SDA
Text GLabel 3100 2150 2    50   Input ~ 0
SCL
Wire Wire Line
	4200 2400 4550 2400
Wire Wire Line
	4200 2500 4550 2500
Wire Wire Line
	4550 2600 3850 2600
Wire Wire Line
	3850 2600 3850 2800
Wire Wire Line
	3850 2800 3100 2800
Text GLabel 3100 2900 2    50   Input ~ 0
5V
Text GLabel 3100 2700 2    50   Input ~ 0
GND
Text GLabel 3350 3450 2    50   Input ~ 0
3V3
Wire Wire Line
	3350 3150 3100 3150
Text GLabel 3350 3150 2    50   Input ~ 0
GND
Wire Wire Line
	3350 3250 3100 3250
Text GLabel 3350 3250 2    50   Input ~ 0
SCL
Wire Wire Line
	3350 3350 3100 3350
Text GLabel 3350 3350 2    50   Input ~ 0
SDA
Wire Wire Line
	3350 3450 3100 3450
Wire Wire Line
	5800 3000 5050 3000
Wire Wire Line
	5050 3100 5800 3100
Wire Wire Line
	5050 3300 5500 3300
Wire Wire Line
	5500 3300 5500 3200
Wire Wire Line
	5500 3200 5800 3200
Wire Wire Line
	5050 3400 5600 3400
Wire Wire Line
	5600 3400 5600 3300
Wire Wire Line
	5600 3300 5800 3300
Text GLabel 5600 2900 0    50   Input ~ 0
GND
Text GLabel 5600 2800 0    50   Input ~ 0
5V
Wire Wire Line
	5600 2800 5800 2800
Wire Wire Line
	5600 2900 5700 2900
Text GLabel 1850 2900 1    50   Input ~ 0
5V
$Comp
L Device:R R4
U 1 1 62107925
P 1850 3300
F 0 "R4" H 1920 3346 50  0000 L CNN
F 1 "1K" H 1920 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1780 3300 50  0001 C CNN
F 3 "~" H 1850 3300 50  0001 C CNN
	1    1850 3300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 621079EF
P 4400 1300
F 0 "#PWR0101" H 4400 1050 50  0001 C CNN
F 1 "GND" H 4405 1127 50  0000 C CNN
F 2 "" H 4400 1300 50  0001 C CNN
F 3 "" H 4400 1300 50  0001 C CNN
	1    4400 1300
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0102
U 1 1 62107D72
P 4400 950
F 0 "#PWR0102" H 4400 800 50  0001 C CNN
F 1 "VCC" H 4417 1123 50  0000 C CNN
F 2 "" H 4400 950 50  0001 C CNN
F 3 "" H 4400 950 50  0001 C CNN
	1    4400 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 950  4400 1050
Connection ~ 4400 1150
$Comp
L power:GND #PWR0103
U 1 1 62108130
P 1850 4550
F 0 "#PWR0103" H 1850 4300 50  0001 C CNN
F 1 "GND" H 1855 4377 50  0000 C CNN
F 2 "" H 1850 4550 50  0001 C CNN
F 3 "" H 1850 4550 50  0001 C CNN
	1    1850 4550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R3
U 1 1 62108178
P 1600 3300
F 0 "R3" H 1670 3346 50  0000 L CNN
F 1 "1K" H 1670 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1530 3300 50  0001 C CNN
F 3 "~" H 1600 3300 50  0001 C CNN
	1    1600 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 6210853C
P 1350 3300
F 0 "R2" H 1420 3346 50  0000 L CNN
F 1 "1K" H 1420 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1280 3300 50  0001 C CNN
F 3 "~" H 1350 3300 50  0001 C CNN
	1    1350 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 62108907
P 1100 3300
F 0 "R1" H 1170 3346 50  0000 L CNN
F 1 "1K" H 1170 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 1030 3300 50  0001 C CNN
F 3 "~" H 1100 3300 50  0001 C CNN
	1    1100 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2900 1850 3000
Wire Wire Line
	1850 3000 1600 3000
Wire Wire Line
	1600 3000 1600 3150
Connection ~ 1850 3000
Wire Wire Line
	1850 3000 1850 3150
Wire Wire Line
	1600 3000 1350 3000
Wire Wire Line
	1350 3000 1350 3150
Connection ~ 1600 3000
Wire Wire Line
	1350 3000 1100 3000
Wire Wire Line
	1100 3000 1100 3150
Connection ~ 1350 3000
$Comp
L Connector:Conn_01x02_Female J11
U 1 1 6210AD7E
P 2050 3850
F 0 "J11" H 2077 3826 50  0000 L CNN
F 1 "SW_Light" V 1950 3350 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 2050 3850 50  0001 C CNN
F 3 "~" H 2050 3850 50  0001 C CNN
	1    2050 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2800 4550 2800
$Comp
L Connector:Conn_01x02_Female J10
U 1 1 621104B9
P 1800 3850
F 0 "J10" H 1827 3826 50  0000 L CNN
F 1 "SW_Fan" V 1700 3450 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1800 3850 50  0001 C CNN
F 3 "~" H 1800 3850 50  0001 C CNN
	1    1800 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J9
U 1 1 62110802
P 1550 3850
F 0 "J9" H 1578 3826 50  0000 L CNN
F 1 "SW_Humi" V 1450 3400 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1550 3850 50  0001 C CNN
F 3 "~" H 1550 3850 50  0001 C CNN
	1    1550 3850
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Female J8
U 1 1 62110867
P 1300 3850
F 0 "J8" H 1327 3826 50  0000 L CNN
F 1 "SW_Door" V 1200 3400 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 1300 3850 50  0001 C CNN
F 3 "~" H 1300 3850 50  0001 C CNN
	1    1300 3850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 3450 1100 3700
Wire Wire Line
	1350 3450 1350 3650
Wire Wire Line
	1600 3450 1600 3600
Wire Wire Line
	1850 3950 1850 4450
Wire Wire Line
	1600 3950 1600 4450
Wire Wire Line
	1600 4450 1850 4450
Connection ~ 1850 4450
Wire Wire Line
	1850 4450 1850 4550
Wire Wire Line
	1350 3950 1350 4450
Wire Wire Line
	1350 4450 1600 4450
Connection ~ 1600 4450
Wire Wire Line
	1100 3950 1100 4450
Wire Wire Line
	1100 4450 1350 4450
Connection ~ 1350 4450
Wire Wire Line
	1850 3450 1850 3550
Wire Wire Line
	4150 2800 4150 3550
Wire Wire Line
	4150 3550 1850 3550
Connection ~ 1850 3550
Wire Wire Line
	1850 3550 1850 3850
Wire Wire Line
	1600 3600 4200 3600
Wire Wire Line
	4200 3600 4200 2900
Wire Wire Line
	4200 2900 4550 2900
Connection ~ 1600 3600
Wire Wire Line
	1600 3600 1600 3850
Wire Wire Line
	1350 3650 4250 3650
Wire Wire Line
	4250 3650 4250 3000
Wire Wire Line
	4250 3000 4550 3000
Connection ~ 1350 3650
Wire Wire Line
	1350 3650 1350 3850
Wire Wire Line
	1100 3700 4300 3700
Wire Wire Line
	4300 3700 4300 3200
Wire Wire Line
	4300 3200 4550 3200
Connection ~ 1100 3700
Wire Wire Line
	1100 3700 1100 3850
$Comp
L Connector:Conn_01x06_Female Door1
U 1 1 6211AB46
P 2900 4500
F 0 "Door1" H 2794 3975 50  0000 C CNN
F 1 "Motor" H 2794 4066 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x06_P2.54mm_Vertical" H 2900 4500 50  0001 C CNN
F 3 "~" H 2900 4500 50  0001 C CNN
	1    2900 4500
	-1   0    0    1   
$EndComp
Text GLabel 3400 4700 2    50   Input ~ 0
5V
Text GLabel 3400 4600 2    50   Input ~ 0
GND
Wire Wire Line
	3400 4600 3100 4600
Wire Wire Line
	3400 4700 3100 4700
Wire Wire Line
	4550 3700 4350 3700
Wire Wire Line
	4350 3700 4350 4500
Wire Wire Line
	4350 4500 3100 4500
Wire Wire Line
	4550 3800 4400 3800
Wire Wire Line
	4400 3800 4400 4400
Wire Wire Line
	4400 4400 3100 4400
Wire Wire Line
	4550 4100 4300 4100
Wire Wire Line
	4300 4100 4300 4300
Wire Wire Line
	4300 4300 3100 4300
Wire Wire Line
	4550 3400 4450 3400
Wire Wire Line
	4450 3400 4450 4200
Wire Wire Line
	4450 4200 3100 4200
$Comp
L Connector:Conn_01x03_Female J12
U 1 1 62128691
P 7100 4150
F 0 "J12" H 7127 4176 50  0000 L CNN
F 1 "5V_GPIO7" H 7127 4085 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 7100 4150 50  0001 C CNN
F 3 "~" H 7100 4150 50  0001 C CNN
	1    7100 4150
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Female J3
U 1 1 6212A2FE
P 6000 2450
F 0 "J3" H 6100 2400 50  0000 C CNN
F 1 "3V3_GIPO15" H 6250 2500 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6000 2450 50  0001 C CNN
F 3 "~" H 6000 2450 50  0001 C CNN
	1    6000 2450
	1    0    0    -1  
$EndComp
Text GLabel 6800 4450 3    50   Input ~ 0
GND
Wire Wire Line
	6800 4450 6800 4250
Wire Wire Line
	6800 4250 6900 4250
Text GLabel 6800 3900 1    50   Input ~ 0
5V
Wire Wire Line
	6800 3900 6800 4050
Wire Wire Line
	6800 4050 6900 4050
Wire Wire Line
	5700 2900 5700 2550
Wire Wire Line
	5700 2550 5800 2550
Connection ~ 5700 2900
Wire Wire Line
	5700 2900 5800 2900
Text GLabel 5700 2200 1    50   Input ~ 0
3V3
Wire Wire Line
	5700 2200 5700 2350
Wire Wire Line
	5700 2350 5800 2350
NoConn ~ 5050 2300
NoConn ~ 5050 2400
NoConn ~ 5050 2500
NoConn ~ 5050 2900
NoConn ~ 5050 3200
NoConn ~ 5050 3600
NoConn ~ 5050 3700
NoConn ~ 4550 4000
NoConn ~ 4550 3900
NoConn ~ 4550 3600
NoConn ~ 4550 3500
NoConn ~ 4550 3300
NoConn ~ 4550 3100
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 621767B5
P 4750 950
F 0 "#FLG0101" H 4750 1025 50  0001 C CNN
F 1 "PWR_FLAG" H 4750 1124 50  0000 C CNN
F 2 "" H 4750 950 50  0001 C CNN
F 3 "~" H 4750 950 50  0001 C CNN
	1    4750 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	4750 950  4750 1050
Wire Wire Line
	4750 1050 4400 1050
Connection ~ 4400 1050
Wire Wire Line
	4400 1050 4400 1150
$Comp
L power:PWR_FLAG #FLG0102
U 1 1 6217FBB3
P 4750 1300
F 0 "#FLG0102" H 4750 1375 50  0001 C CNN
F 1 "PWR_FLAG" H 4750 1473 50  0000 C CNN
F 2 "" H 4750 1300 50  0001 C CNN
F 3 "~" H 4750 1300 50  0001 C CNN
	1    4750 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4400 1250 4750 1250
Wire Wire Line
	4750 1250 4750 1300
Wire Wire Line
	4400 1250 4400 1300
$Comp
L Connector:Conn_01x04_Female Fan1
U 1 1 62182A35
P 2900 1700
F 0 "Fan1" H 2794 1275 50  0000 C CNN
F 1 "Fan" H 2794 1366 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2900 1700 50  0001 C CNN
F 3 "~" H 2900 1700 50  0001 C CNN
	1    2900 1700
	-1   0    0    1   
$EndComp
Text GLabel 5150 1050 0    50   Input ~ 0
12V
Text GLabel 3350 1800 2    50   Input ~ 0
12V
Wire Wire Line
	3350 1800 3100 1800
Text GLabel 3350 1500 2    50   Input ~ 0
GND
Wire Wire Line
	3350 1700 3100 1700
Text GLabel 5400 2100 1    50   Input ~ 0
PWM0_0
Text GLabel 5300 2100 1    50   Input ~ 0
PWM0_1
Wire Wire Line
	5400 2100 5400 2800
Wire Wire Line
	5050 2800 5400 2800
Text GLabel 3350 1700 2    50   Input ~ 0
PWM0_0
Text GLabel 3350 1600 2    50   Input ~ 0
PWM0_1
Wire Wire Line
	3350 1600 3100 1600
Wire Wire Line
	3350 1500 3100 1500
Wire Wire Line
	5050 2600 5300 2600
Wire Wire Line
	5300 2600 5300 2100
$Comp
L Mechanical:MountingHole H1
U 1 1 6212260F
P 1700 1300
F 0 "H1" H 1800 1346 50  0000 L CNN
F 1 "MountingHole" H 1800 1255 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1700 1300 50  0001 C CNN
F 3 "~" H 1700 1300 50  0001 C CNN
	1    1700 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 62122689
P 1700 1450
F 0 "H2" H 1800 1496 50  0000 L CNN
F 1 "MountingHole" H 1800 1405 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1700 1450 50  0001 C CNN
F 3 "~" H 1700 1450 50  0001 C CNN
	1    1700 1450
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 621226E9
P 1700 1600
F 0 "H3" H 1800 1646 50  0000 L CNN
F 1 "MountingHole" H 1800 1555 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1700 1600 50  0001 C CNN
F 3 "~" H 1700 1600 50  0001 C CNN
	1    1700 1600
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 62122749
P 1700 1750
F 0 "H4" H 1800 1796 50  0000 L CNN
F 1 "MountingHole" H 1800 1705 50  0000 L CNN
F 2 "MountingHole:MountingHole_2.7mm_M2.5" H 1700 1750 50  0001 C CNN
F 3 "~" H 1700 1750 50  0001 C CNN
	1    1700 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	5150 1050 5650 1050
Connection ~ 4750 1250
Wire Wire Line
	4400 1250 4200 1250
Connection ~ 4400 1250
Wire Wire Line
	5050 2700 5550 2700
Wire Wire Line
	5550 2700 5550 2450
Wire Wire Line
	5550 2450 5800 2450
NoConn ~ 3100 950 
NoConn ~ 3100 850 
Wire Wire Line
	3450 1050 3100 1050
Wire Wire Line
	3450 750  3100 750 
Text GLabel 3450 750  2    50   Input ~ 0
GND
Text GLabel 3450 1050 2    50   Input ~ 0
5V
$Comp
L Connector:Conn_01x04_Female J14
U 1 1 6215C407
P 2900 950
F 0 "J14" H 2794 525 50  0000 C CNN
F 1 "USB_4P" H 2794 616 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2900 950 50  0001 C CNN
F 3 "~" H 2900 950 50  0001 C CNN
	1    2900 950 
	-1   0    0    1   
$EndComp
Text GLabel 7050 2700 0    50   Input ~ 0
SDA
Text GLabel 7950 2700 2    50   Input ~ 0
SCL
Text GLabel 4900 4850 0    50   Input ~ 0
ENA+
Text GLabel 4900 5050 0    50   Input ~ 0
DIR+
Text GLabel 4900 5250 0    50   Input ~ 0
PUL+
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J6
U 1 1 621059FB
P 4750 3200
F 0 "J6" H 4800 4317 50  0000 C CNN
F 1 "PI_GPIO" H 4800 4226 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 4750 3200 50  0001 C CNN
F 3 "~" H 4750 3200 50  0001 C CNN
	1    4750 3200
	1    0    0    -1  
$EndComp
Text GLabel 5400 4000 2    50   Input ~ 0
ENA+
Text GLabel 5400 4100 2    50   Input ~ 0
DIR+
Text GLabel 5400 4200 2    50   Input ~ 0
PUL+
Wire Wire Line
	5400 4000 5050 4000
Wire Wire Line
	5400 4100 5050 4100
Wire Wire Line
	5400 4200 5050 4200
Wire Wire Line
	5050 3500 6650 3500
Wire Wire Line
	6650 3500 6650 4150
Wire Wire Line
	6650 4150 6900 4150
Wire Wire Line
	7950 2700 7750 2700
Wire Wire Line
	7050 2700 7250 2700
$Comp
L Connector:Conn_01x01_Female Shade1
U 1 1 6251789B
P 6100 4550
F 0 "Shade1" H 6128 4576 50  0000 L CNN
F 1 "Shade_Zeroing" H 6128 4485 50  0000 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x01_P2.54mm_Vertical" H 6100 4550 50  0001 C CNN
F 3 "~" H 6100 4550 50  0001 C CNN
	1    6100 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3800 5900 3800
Wire Wire Line
	5900 3800 5900 4550
$Comp
L Device:R R6
U 1 1 625329B9
P 7850 2200
F 0 "R6" H 7920 2246 50  0000 L CNN
F 1 "4.7K" H 7920 2155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7780 2200 50  0001 C CNN
F 3 "~" H 7850 2200 50  0001 C CNN
	1    7850 2200
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 62532C12
P 7550 2200
F 0 "R5" H 7620 2246 50  0000 L CNN
F 1 "4.7K" H 7620 2155 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7480 2200 50  0001 C CNN
F 3 "~" H 7550 2200 50  0001 C CNN
	1    7550 2200
	1    0    0    -1  
$EndComp
Text GLabel 7950 1950 2    50   Input ~ 0
3V3
Wire Wire Line
	7950 1950 7850 1950
Wire Wire Line
	7850 1950 7850 2050
Wire Wire Line
	7850 1950 7550 1950
Wire Wire Line
	7550 1950 7550 2050
Connection ~ 7850 1950
Wire Wire Line
	7550 2350 7250 2350
Wire Wire Line
	7250 2350 7250 2700
Wire Wire Line
	7850 2350 7750 2350
Wire Wire Line
	7750 2350 7750 2700
$Comp
L Connector:Conn_01x04_Female SHT30
U 1 1 626A2B3A
P 2900 5350
F 0 "SHT30" H 2794 4925 50  0000 C CNN
F 1 "SHT30" H 2794 5016 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Vertical" H 2900 5350 50  0001 C CNN
F 3 "~" H 2900 5350 50  0001 C CNN
	1    2900 5350
	-1   0    0    1   
$EndComp
Text GLabel 3300 5150 2    50   Input ~ 0
GND
Text GLabel 3300 5250 2    50   Input ~ 0
SCL
Text GLabel 3300 5350 2    50   Input ~ 0
SDA
Text GLabel 3300 5450 2    50   Input ~ 0
5V
Wire Wire Line
	3300 5150 3100 5150
Wire Wire Line
	3300 5250 3100 5250
Wire Wire Line
	3300 5350 3100 5350
Wire Wire Line
	3300 5450 3100 5450
$Comp
L Connector:Conn_01x03_Female DC_IN1
U 1 1 62BB54F5
P 5850 1150
F 0 "DC_IN1" H 5878 1176 50  0000 L CNN
F 1 "Power_12v_5v" H 5878 1085 50  0000 L CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x03_P3.50mm_Horizontal" H 5850 1150 50  0001 C CNN
F 3 "~" H 5850 1150 50  0001 C CNN
	1    5850 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 1150 5650 1150
Wire Wire Line
	4750 1250 5650 1250
Entry Bus Bus
	6050 7750 6150 7850
$Comp
L Connector:Conn_01x02_Male I2C1
U 1 1 62BDFA5F
P 7450 2900
F 0 "I2C1" V 7603 2712 50  0000 R CNN
F 1 "I2C" V 7512 2712 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 7450 2900 50  0001 C CNN
F 3 "~" H 7450 2900 50  0001 C CNN
	1    7450 2900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7750 2700 7550 2700
Connection ~ 7750 2700
Wire Wire Line
	7450 2700 7250 2700
Connection ~ 7250 2700
Text GLabel 4200 2700 0    50   Input ~ 0
GND
Wire Wire Line
	4200 2700 4550 2700
$Comp
L hAI-rescue:USB_TYPE_A-USB-TYPE-A J1
U 1 1 62BF8B63
P 6800 1050
F 0 "J1" H 7200 1315 50  0000 C CNN
F 1 "USB_TYPE_A" H 7200 1224 50  0000 C CNN
F 2 "62900416021:62900416021" H 7450 1150 50  0001 L CNN
F 3 "http://katalog.we-online.de/em/datasheet/62900416021.pdf" H 7450 1050 50  0001 L CNN
F 4 "Wurth Elektronik Female 1 Port Right Angle SMT Version 2 Type A USB Connector, 30 V ac, 1.5A WR-COM 62900" H 7450 950 50  0001 L CNN "Description"
F 5 "7" H 7450 850 50  0001 L CNN "Height"
F 6 "Wurth Elektronik" H 7450 750 50  0001 L CNN "Manufacturer_Name"
F 7 "62900416021" H 7450 650 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "710-62900416021" H 7450 550 50  0001 L CNN "Mouser Part Number"
F 9 "https://www.mouser.co.uk/ProductDetail/Wurth-Elektronik/62900416021?qs=7gQLVZk5cPluNNYrEeR0bw%3D%3D" H 7450 450 50  0001 L CNN "Mouser Price/Stock"
	1    6800 1050
	1    0    0    -1  
$EndComp
Text GLabel 7950 1050 2    50   Input ~ 0
5V
$Comp
L power:GND #PWR01
U 1 1 62BF8E5F
P 7200 1600
F 0 "#PWR01" H 7200 1350 50  0001 C CNN
F 1 "GND" H 7205 1427 50  0000 C CNN
F 2 "" H 7200 1600 50  0001 C CNN
F 3 "" H 7200 1600 50  0001 C CNN
	1    7200 1600
	1    0    0    -1  
$EndComp
Wire Wire Line
	7600 1350 7600 1600
Wire Wire Line
	7600 1600 7200 1600
Wire Wire Line
	6800 1150 6800 1600
Wire Wire Line
	6800 1600 7200 1600
Connection ~ 7200 1600
Wire Wire Line
	6800 1050 6800 1150
Connection ~ 6800 1150
Wire Wire Line
	7950 1050 7600 1050
NoConn ~ 7600 1150
NoConn ~ 7600 1250
$Comp
L Regulator_Linear:LM1117-3.3 U1
U 1 1 62D100FC
P 8350 3500
F 0 "U1" H 8350 3742 50  0000 C CNN
F 1 "LM1117-3.3" H 8350 3651 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 8350 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/lm1117.pdf" H 8350 3500 50  0001 C CNN
	1    8350 3500
	1    0    0    -1  
$EndComp
Text GLabel 7550 3500 0    50   Input ~ 0
5V
Text GLabel 7800 3950 0    50   Input ~ 0
GND
Wire Wire Line
	7800 3950 7900 3950
Wire Wire Line
	8350 3950 8350 3800
Wire Wire Line
	8050 3500 7900 3500
Text GLabel 9150 3500 2    50   Input ~ 0
3V3
$Comp
L Device:C C1
U 1 1 62D1AD15
P 7900 3650
F 0 "C1" H 8015 3696 50  0000 L CNN
F 1 "10uF/50V" H 7500 3550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 7938 3500 50  0001 C CNN
F 3 "~" H 7900 3650 50  0001 C CNN
	1    7900 3650
	1    0    0    -1  
$EndComp
$Comp
L Device:C C2
U 1 1 62D1ADB2
P 8900 3650
F 0 "C2" H 9015 3696 50  0000 L CNN
F 1 "22uf/50V" H 9000 3550 50  0000 L CNN
F 2 "Capacitor_THT:C_Disc_D5.0mm_W2.5mm_P5.00mm" H 8938 3500 50  0001 C CNN
F 3 "~" H 8900 3650 50  0001 C CNN
	1    8900 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 3500 8900 3500
Connection ~ 8900 3500
Wire Wire Line
	8900 3500 9150 3500
Wire Wire Line
	8900 3800 8900 3950
Wire Wire Line
	8900 3950 8350 3950
Connection ~ 8350 3950
Connection ~ 7900 3950
Wire Wire Line
	7900 3950 8350 3950
Connection ~ 7900 3500
Wire Wire Line
	7900 3500 7550 3500
Wire Wire Line
	7900 3800 7900 3950
NoConn ~ 4550 2300
$Comp
L Connector:Conn_01x03_Female ExP1
U 1 1 62D362A4
P 5250 5650
F 0 "ExP1" H 5278 5676 50  0000 L CNN
F 1 "ExPower" H 5278 5585 50  0000 L CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x03_P3.50mm_Horizontal" H 5250 5650 50  0001 C CNN
F 3 "~" H 5250 5650 50  0001 C CNN
	1    5250 5650
	1    0    0    -1  
$EndComp
Text GLabel 4900 5650 0    50   Input ~ 0
GND
Text GLabel 4900 5750 0    50   Input ~ 0
12V
Text GLabel 4900 5550 0    50   Input ~ 0
3V3
Wire Wire Line
	4900 5750 5050 5750
$Comp
L Connector:Conn_01x06_Female TB6600
U 1 1 62D42621
P 5250 5050
F 0 "TB6600" H 5278 5026 50  0000 L CNN
F 1 "TB6600" H 5278 4935 50  0000 L CNN
F 2 "TerminalBlock_4Ucon:TerminalBlock_4Ucon_1x06_P3.50mm_Horizontal" H 5250 5050 50  0001 C CNN
F 3 "~" H 5250 5050 50  0001 C CNN
	1    5250 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 5550 5050 5550
Wire Wire Line
	4900 4850 5050 4850
Wire Wire Line
	4900 5050 5050 5050
Wire Wire Line
	4900 5250 5050 5250
Wire Wire Line
	4950 4950 4950 5150
Wire Wire Line
	4900 5650 4950 5650
Connection ~ 4950 5650
Wire Wire Line
	4950 5650 5050 5650
Wire Wire Line
	5050 4950 4950 4950
Wire Wire Line
	5050 5150 4950 5150
Connection ~ 4950 5150
Wire Wire Line
	4950 5150 4950 5350
Wire Wire Line
	5050 5350 4950 5350
Connection ~ 4950 5350
Wire Wire Line
	4950 5350 4950 5650
NoConn ~ 5050 3900
NoConn ~ 4550 4200
$Comp
L Connector:Conn_01x02_Male Fan12V1
U 1 1 62DDBE30
P 6300 5150
F 0 "Fan12V1" H 6406 5328 50  0000 C CNN
F 1 "12V_Fan" H 6406 5237 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x02_P2.54mm_Vertical" H 6300 5150 50  0001 C CNN
F 3 "~" H 6300 5150 50  0001 C CNN
	1    6300 5150
	1    0    0    -1  
$EndComp
Text GLabel 6750 5150 2    50   Input ~ 0
12V
Text GLabel 6750 5250 2    50   Input ~ 0
GND
Wire Wire Line
	6500 5150 6750 5150
Wire Wire Line
	6500 5250 6750 5250
$Comp
L Device:CP1_Small C?
U 1 1 62DE9512
P 9500 3700
F 0 "C?" H 9591 3746 50  0000 L CNN
F 1 "CP1_Small" H 9591 3655 50  0000 L CNN
F 2 "" H 9500 3700 50  0001 C CNN
F 3 "~" H 9500 3700 50  0001 C CNN
	1    9500 3700
	1    0    0    -1  
$EndComp
$Comp
L Device:CP1_Small C?
U 1 1 62DE95C1
P 7700 3300
F 0 "C?" H 7791 3346 50  0000 L CNN
F 1 "CP1_Small" H 7791 3255 50  0000 L CNN
F 2 "" H 7700 3300 50  0001 C CNN
F 3 "~" H 7700 3300 50  0001 C CNN
	1    7700 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 62DE9685
P 9250 4100
F 0 "D?" V 9288 3983 50  0000 R CNN
F 1 "LED" V 9197 3983 50  0000 R CNN
F 2 "" H 9250 4100 50  0001 C CNN
F 3 "~" H 9250 4100 50  0001 C CNN
	1    9250 4100
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R?
U 1 1 62DE97AE
P 9250 4550
F 0 "R?" H 9320 4596 50  0000 L CNN
F 1 "R" H 9320 4505 50  0000 L CNN
F 2 "" V 9180 4550 50  0001 C CNN
F 3 "~" H 9250 4550 50  0001 C CNN
	1    9250 4550
	1    0    0    -1  
$EndComp
$EndSCHEMATC
